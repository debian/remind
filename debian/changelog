remind (05.03.04-1) unstable; urgency=medium

  * New upstream version 05.03.04

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 11 Mar 2025 09:14:31 +0100

remind (05.03.03-1) unstable; urgency=medium

  * New upstream version 05.03.03
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 03 Mar 2025 17:52:16 +0100

remind (05.03.02-1) unstable; urgency=medium

  * New upstream version 05.03.02

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 09 Feb 2025 18:53:30 +0100

remind (05.03.01-1) unstable; urgency=medium

  * New upstream version 05.03.01

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 07 Feb 2025 19:23:39 +0100

remind (05.03.00-1) unstable; urgency=medium

  * New upstream version 05.03.00

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 04 Feb 2025 20:42:18 +0100

remind (05.02.03-1) unstable; urgency=medium

  * New upstream version 05.02.03
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 22 Jan 2025 18:08:08 +0100

remind (05.02.02-1) unstable; urgency=medium

  * New upstream version 05.02.02
  * Update d/copyright
  * Update README filename

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 07 Jan 2025 09:13:05 +0100

remind (05.02.01-1) unstable; urgency=medium

  * New upstream version 05.02.01

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 17 Dec 2024 09:12:56 +0100

remind (05.02.00-1) unstable; urgency=medium

  * New upstream version 05.02.00

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 16 Dec 2024 15:52:04 +0100

remind (05.01.01-1) unstable; urgency=medium

  * New upstream version 05.01.01
  * Drop 0002-Fix-remind-call-in-test.patch: Fixed upstream

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 18 Nov 2024 17:14:18 +0100

remind (05.01.00-1) unstable; urgency=medium

  * New upstream version 05.01.00
  * Update d/copyright
  * Drop 0002-Fix-autoreconf.patch: Fixed upstream
  * Fix `remind` call in test

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 18 Nov 2024 16:26:29 +0100

remind (05.00.07-1) unstable; urgency=medium

  * New upstream version 05.00.07
  * Update d/copyright
  * Fix autoreconf

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 16 Oct 2024 22:19:12 +0200

remind (05.00.06-1) unstable; urgency=medium

  * New upstream version 05.00.06
  * Update d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 16 Sep 2024 16:37:18 +0200

remind (05.00.05-1) unstable; urgency=medium

  * New upstream version 05.00.05

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 02 Sep 2024 21:17:56 +0200

remind (05.00.04-1) unstable; urgency=medium

  * New upstream version 05.00.04

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 29 Aug 2024 14:33:00 +0200

remind (05.00.03-1) unstable; urgency=medium

  * New upstream version 05.00.03
  * Drop Fix typo in manpage patch (merged upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 29 Aug 2024 07:08:21 +0200

remind (05.00.02-1) unstable; urgency=medium

  * New upstream version 05.00.02
  * Fix typo in manpage

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 26 Jul 2024 14:17:48 +0200

remind (05.00.01-1) unstable; urgency=medium

  * New upstream version 05.00.01

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 08 Jun 2024 21:53:41 +0200

remind (05.00.00-1) unstable; urgency=medium

  * New upstream version 05.00.00

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 06 Jun 2024 17:02:56 +0200

remind (04.03.07-1) unstable; urgency=medium

  * New upstream version 04.03.07
  * Bump policy version (no changes)
  * Stop disabling failing tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 30 Apr 2024 05:00:58 +0200

remind (04.03.06-1) unstable; urgency=medium

  * New upstream version 04.03.06
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 02 Apr 2024 15:16:21 +0200

remind (04.03.04-1) unstable; urgency=medium

  * New upstream version 04.03.04
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 25 Mar 2024 16:43:01 +0100

remind (04.03.03-1) unstable; urgency=medium

  * New upstream version 04.03.03
  * Rediff patches
  * Update tkremind install dirs (fixed upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 25 Mar 2024 09:20:49 +0100

remind (04.03.01-1) unstable; urgency=medium

  * New upstream version 04.03.01
  * Rediff patches
  * Install desktop file (Closes: #694648)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 01 Mar 2024 09:46:49 +0100

remind (04.02.09-1) unstable; urgency=medium

  * New upstream version 04.02.09
  * Update d/copyright
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 05 Feb 2024 06:29:08 +0100

remind (04.02.08-1) unstable; urgency=medium

  * New upstream version 04.02.08
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 14 Dec 2023 22:35:28 +0100

remind (04.02.07-1) unstable; urgency=medium

  * New upstream version 04.02.07

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 09 Oct 2023 16:37:52 +0200

remind (04.02.06-1) unstable; urgency=medium

  * Take over maintainership, thanks Ana for your work!
  * New upstream version 04.02.06
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 12 Sep 2023 21:03:18 +0200

remind (04.02.05-1) unstable; urgency=medium

  * New upstream version 04.02.05
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 11 Jun 2023 10:48:06 +0200

remind (04.02.03-4) unstable; urgency=medium

  * Add upstream solution for reproducible TZ in tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 23 Feb 2023 15:00:38 +0100

remind (04.02.03-3) unstable; urgency=medium

  * unexport TZ to fix tests in reproducible builds

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 23 Feb 2023 09:19:20 +0100

remind (04.02.03-2) unstable; urgency=medium

  * Add upstream patch to avoid segfault
  * Revert "Drop locale exports (fixed upstream)"
  * Add upstream patch to show test errors

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 22 Feb 2023 22:14:13 +0100

remind (04.02.03-1) unstable; urgency=medium

  * New upstream version 04.02.03
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 10 Feb 2023 19:54:27 +0100

remind (04.02.02-3) unstable; urgency=medium

  * Add missing build dependency on tzdata (Closes: #1029463)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 23 Jan 2023 08:07:52 +0100

remind (04.02.02-2) unstable; urgency=medium

  * Remove tests failing on 32bit

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 02 Jan 2023 10:20:47 +0100

remind (04.02.02-1) unstable; urgency=medium

  * Drop old Debian menu file
  * New upstream version 04.02.02
  * Update d/copyright
  * Bump policy version (no changes)
  * Rebase patch
  * Improve package description

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 02 Jan 2023 08:32:32 +0100

remind (04.02.01-1) unstable; urgency=medium

  * New upstream version 04.02.01

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 15 Dec 2022 19:22:35 +0100

remind (04.02.00-1) unstable; urgency=medium

  * New upstream version 04.02.00
  * Drop locale exports (fixed upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 14 Oct 2022 17:17:40 +0200

remind (04.01.00-1) unstable; urgency=medium

  * New upstream version 04.01.00
  * Rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 26 Sep 2022 20:16:09 +0200

remind (04.00.03-1) unstable; urgency=medium

  * New upstream version 04.00.03

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 16 Aug 2022 16:54:45 +0200

remind (04.00.02-1) unstable; urgency=medium

  * New upstream version 04.00.02

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 02 Aug 2022 21:31:36 +0200

remind (04.00.01-1) unstable; urgency=medium

  * Add upstream source
  * New upstream version 04.00.01
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 04 Jun 2022 10:15:52 +0200

remind (04.00.00-1) unstable; urgency=medium

  * Fix tests with pbuilder
  * New upstream version 04.00.00
  * Drop -fno-lto
  * rebase patches
  * Install language packs

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 04 Apr 2022 16:18:15 +0200

remind (03.04.02-1) unstable; urgency=medium

  * New upstream version 03.04.02
  * Update d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 14 Mar 2022 22:56:20 +0100

remind (03.04.01-4) unstable; urgency=medium

  * Add Breaks+Replaces: remind (<< 03.04.01-2)
    Thanks to Andreas Beckmann (Closes: #1007229)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 14 Mar 2022 08:37:42 +0100

remind (03.04.01-3) unstable; urgency=medium

  * source only upload

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Mar 2022 12:06:58 +0100

remind (03.04.01-2) unstable; urgency=medium

  * Move description to source package
  * Add remind-tools package
  * Drop old README.Debian

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Mar 2022 08:49:04 +0100

remind (03.04.01-1) unstable; urgency=medium

  * New upstream version 03.04.01
  * Rebase patch

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 23 Feb 2022 18:10:09 +0100

remind (03.04.00-2) unstable; urgency=medium

  * Disable lto for Ubuntu

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 18 Feb 2022 09:42:10 +0100

remind (03.04.00-1) unstable; urgency=medium

  * New upstream version 03.04.00
  * Rebase patches
  * Install rem2html manpage
  * Install rem2pdf

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 10 Feb 2022 23:59:33 +0100

remind (03.03.12-1) unstable; urgency=medium

  [ наб ]
  * Add patch and Build-Depends to use system libjsonparser (Closes: #958256)

  [ Jochen Sprickerhof ]
  * Update d/watch
  * New upstream version 03.03.12 (Closes: #992493)
  * Add myself to Uploaders (with maintainer permission)
  * Rebase patch
  * Add patches for the build system
  * Remove trailing space in d/changelog (lintian)
  * Rewrite d/copyright in DEP5
  * Make tkremind arch all
  * Don't install test files
  * Drop cm2rem (dropped upstream)
  * Switch to dh and bump compat and policy
  * Install rem2html
  * tkremind depends on inotify-tools for inotifywait

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 09 Feb 2022 08:36:53 +0100

remind (03.03.01-1) unstable; urgency=medium

  * New upstream release.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Tue, 14 Apr 2020 23:47:14 +0200

remind (03.03.00-1) unstable; urgency=medium

  * New upstream release:
    - Add support for events spanning multiple days (Closes: #529277)
    - sunrise() and sunset() should work now (Closes: #742705)
  * Remove patch manpage_fix_typo, fixed upstream
  * Refresh patch do-not-strip.diff
  * Update to Standards-Version 4.5.0, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sun, 09 Feb 2020 14:50:14 +0100

remind (03.01.16-1) unstable; urgency=medium

  * New upstream release.
  * Update homepage.
  * Update to Standards-Version 4.3.0, no changes required.
  * Include the directory contrib with the examples. (Closes: #802173)

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 13 Feb 2019 00:35:48 +0100

remind (03.01.15-2) unstable; urgency=medium

  * Fix upstream's name. (Closes: #883740)
  * Bump to debhelper compat 10.
  * Update upstream homepage, now uses https.
  * Update to Standards-Version 4.1.4, no changes required.
  * Add Vcs-* fields.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sun, 27 May 2018 22:56:37 +0200

remind (03.01.15-1) unstable; urgency=medium

  * New upstream release.
  * Update to Standards-Version 3.9.6, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 29 Jul 2015 22:59:53 +0200

remind (03.01.14-1) unstable; urgency=medium

  * New upstream release.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 24 Jun 2015 15:41:35 +0200

remind (03.01.13-1) unstable; urgency=low

  * New upstream release.
  * Update to Standards-Version 3.9.4, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 15 Jul 2013 23:08:34 +0200

remind (03.01.12-1) unstable; urgency=low

  * New upstream release.
  * New maintainer. (Closes: #677711)
  * Remove patches merged upstream: manpage-typos.diff, fix_ftbfs,
    manpage-escape-minuses.diff and cleanout-www-makefile.diff.
  * Update to Standards-Version 3.9.3, no changes required.
  * Improve tkremind's description by adding what remind is.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sat, 16 Jun 2012 17:52:57 +0200

remind (03.01.10-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with flag -Werror=format-security. (Closes: #643466)

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sat, 03 Dec 2011 23:10:31 +0100

remind (03.01.10-1) unstable; urgency=low

  * New Upstream Version. (Closes: #573184, #566374, #522068)
  * Relax dependency from tclX.X to tcl. (Closes: #605197)
  * Switch from dpatch to quilt.
  * Update copyright date.
  * Update to dhelper compatibility level 8 and Standards-Version 3.9.1.
  * Switch to source format 3.0 (quilt) and DEP-3 headers.
  * Add ${misc:Depends} to quiet lintian.
  * Add a README.Debian file describing the use of rem. (Closes: #505888)
  * Remove DM flag.
  * Modify description to conform with upstream.

 -- Kurt B. Kaiser <kbk@shore.net>  Sat, 02 Apr 2011 17:27:31 -0400

remind (03.01.05-2) unstable; urgency=low

  * New Upstream Version.
  * Update copyright date, add David F. Skoll '92 - '98 copyright.
  * Version the tkremind dependency on remind.
  * 03.01.05-1 was on mentors.d.o for two months.
  * Switch from XS-DM-Upload-Allowed back to DM-Upload-Allowed.

 -- Kurt B. Kaiser <kbk@shore.net>  Fri, 11 Jul 2008 11:12:07 -0400

remind (03.01.04-1) unstable; urgency=low

  * New Upstream Version. (Closes: #442389)
  * Update watch file. (Closes: #449729)
  * Update home page.
  * Switch to dpatch from cdbs simple patch system.
  * Delete 01-optional filename.dpatch, fixed upstream.
  * Delete 02-correct-substitution.dpatch, fixed upstream.
  * Delete 03-broken-postscript.dpatch, fixed upstream.
  * Delete 3 of 4 hunks of 04-manpage-typos, fixed upstream.
  * Add three more typos to 04-manpage-typos per latest lintian.
    (Closes: #422996 )
  * Delete 05-tkremind-newlines.dpatch, fixed upstream.
  * Delete 06-fix-manpage-errors.dpatch, fixed upstream.
  * Update 07-manpage-escape-minuses.dpatch, mostly not fixed yet.
  * Delete 08-eliminate-rem-kall-in-deb-manpage.dpatch, fixed upstream.
  * Update menu to use new section layout.
  * Upstream was stripping, use install-nostripped target. (Closes: #437902)
  * Upstream requested: split package to make tkremind a suggested package.
    This eliminates the dependency on tcl/tk for the core remind program
    and opens a path for a wxRemind package.
  * Remove README.Debian: not needed, upstream removed 'rem' and 'kall'
    executables and replaced the former with a symlink.
  * DST date change bug fixed upstream. (Closes: #413739)
  * Update Standards Version to 3.7.3.
  * Add DM-Upload-Allowed to debian/control.
  * Make Homepage: a field in debian/control.
  * Update debian/copyright.
  * Change rules to remove www/Makefile in clean - it's not in source tarball.
    (09-cleanout-www-makefile.dpatch should be sent upstream)

 -- Kurt B. Kaiser <kbk@shore.net>  Thu, 28 Feb 2008 14:10:09 -0500

remind (03.00.24-4) unstable; urgency=low

  * New maintainer  (Closes: #388426)
  * Update standards version and debhelper compat level
  * Escape minus signs in manpages
  * Remove mention of 'rem' and 'kall' manpages in remind manpage
  * Remove empty lines at end of debian/rules and debian/changelog
  * Rename debian/remind.menu to debian/menu

 -- Kurt B. Kaiser <kbk@shore.net>  Sun,  8 Oct 2006 19:26:37 -0400

remind (03.00.24-3) unstable; urgency=low

  * debian/copyright: list exceptionial files with different copyright
    holders

  * debian/control: recommend "wyrd" as an alternative to Tcl/Tk, so that
    users can use "wyrd" as alternate frontend to "tkremind". Thanks to
    Thaddeus H. Black for the suggension (closes: #360278)

 -- René van Bevern <rvb@progn.org>  Mon,  3 Apr 2006 14:34:27 +0200

remind (03.00.24-2) unstable; urgency=low

  * Scripts in www/ are examples rather than documentation.
    + debian/docs: remove www
    + debian/examples: add www
    + debian/rules: make examples non-executable

  * debian/control:
    + recommend virtual packages tclsh and wish
    + updated package description

  * debian/watch: added

  * debian/patches/06-fix-manpage-errors.patch: fix some formatting and
    character flaws in manpages remind.1 and cm2rem.1

 -- René van Bevern <rvb@pro-linux.de>  Wed, 14 Dec 2005 16:15:06 +0100

remind (03.00.24-1) unstable; urgency=low

  * New upstream version
  * debian/patches/01-optional-filename.diff: update for new protos.h

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@pro-linux.de>  Sun, 20 Nov 2005 21:18:10 +0100

remind (03.00.23-6) unstable; urgency=low

  * debian/rules: use cdbs
  * debian/patches/03-broken-postscript.patch: rem2ps shall not
    corrupt generated PostScript (closes: #320415)
  * debian/control: Standards-Version: 3.6.2
  * debian/control: Recommend tk8.4 and tcl8.4 instead of Suggest
    wish and tclsh

  * Upload sponsored by Norbert Tretkowski <nobse@debian.org>

 -- René van Bevern <rvb@pro-linux.de>  Fri, 29 Jul 2005 14:27:33 +0200

remind (03.00.23-5) unstable; urgency=low

  * Closes: #312814: FTBFS: missing binary-arch target

 -- René van Bevern <rvb@pro-linux.de>  Fri, 10 Jun 2005 11:37:29 +0200

remind (03.00.23-4) unstable; urgency=low

  * Closes: #311029: manpage typos, patch by A Costa <agcosta@gis.net>
  * Closes: #311030: manpage typos, patch by A Costa <agcosta@gis.net>
  * drop dpatch in favor of Darcs (README.Packaging)
  * add 'debian/docs' and 'debian/examples' instead of writing
    filenames into 'debian/rules'

 -- René van Bevern <rvb@pro-linux.de>  Fri,  3 Jun 2005 13:23:52 +0200

remind (03.00.23-3) unstable; urgency=low

  * filename argument to rename is now optional, rem is dropped,
    see "README.Debian" (Closes: #310818)
  * removed patch against #219636 (was caused by rem)
  * added rem's removal to README.Debian
  * updated manpage to new usage
  * added homepage URL to description

 -- René van Bevern <rvb@pro-linux.de>  Thu, 26 May 2005 20:08:51 +0200

remind (03.00.23-2) unstable; urgency=low

  * Closes: #75883: If using tkremind entering body text w/ newline
    causes error

 -- Rene van Bevern <rvb@pro-linux.de>  Sat, 30 Apr 2005 17:39:44 +0200

remind (03.00.23-1) unstable; urgency=low

  * new maintainer
  * use dpatch
  * Closes: #219636: rem script breaks up multiword arguments
  * Closes: #306292: copyright file wrong
  * Closes: #306295: wrong %s substitution when using -k
  * Closes: #306301: new upstream version and policy update
  * drop "kall" script, which is a duplicate of what "killall" does

 -- Rene van Bevern (RvB) <rvb@pro-linux.de>  Tue, 26 Apr 2005 21:02:05 +0200

remind (03.00.22-4) unstable; urgency=low

  * Fixed build-dep debhelper version.
  * Added tkremind menu entry. (Closes: #120285)
  * Renamed cm2rem.tcl to cm2rem. (Closes: #189538)

 -- Norbert Veber <nveber@pyre.virge.net>  Thu, 18 Sep 2003 23:27:19 -0400

remind (03.00.22-3) unstable; urgency=low

  * This package is not debian native.  Fetched the orginal upstream tar
    and re-built properly.
  * Updated to standards version 3.6.1, and debhelper compatibility
    level 4.

 -- Norbert Veber <nveber@pyre.virge.net>  Wed, 17 Sep 2003 23:18:53 -0400

remind (03.00.22-2) unstable; urgency=low

  * New maintainer (Closes: #210891)
  * NMU ack (Closes: #133872, #42402, #59447)
  * Remind uses local time on my system (Closes: #54493)
  * The packages.debian.org listing of remind is now correct.
    Closes: #77128
  * Changed Suggests: to "tclsh" from "tcl" (Closes: #96279)
  * Removed redundant README files. (Closes: #154758)

 -- Norbert Veber <nveber@pyre.virge.net>  Wed, 17 Sep 2003 13:06:20 -0400

remind (03.00.22-1.1) unstable; urgency=low

  * NMU (bug squashing party).
  * Added debhelper to Build-Depends (closes: #133872).

 -- Sebastien Bacher <seb128@debian.org>  Sat, 31 Aug 2002 15:48:15 +0200

remind (03.00.22-1) unstable; urgency=low

  * NMU upload, maintainer seems to be missing.
  * Changed to main (now GPL) (Closes: #42402)
  * New upstream version (Closes: #59447)
  * Moved to use debconf.

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Fri, 19 Feb 1999 13:36:15 -0500

remind (03.00.19-2) unstable; urgency=low

  * fixed #19399: lintian clean, kinda

 -- Paul Haggart <phaggart@debian.org>  Fri, 19 Feb 1999 13:36:15 -0500

remind (03.00.19-1) unstable; urgency=low

  * new upstream source

 -- Paul Haggart <phaggart@debian.org>  Mon, 18 May 1998 13:39:02 -0400

remind (03.00.18-2) frozen; urgency=low

  * put me back in the archive .. non-free/utils if you would

 -- Paul Haggart <phaggart@debian.org>  Sun, 15 Mar 1998 14:25:18 -0500

remind (03.00.18-1) unstable; urgency=low

  * new upstream version
  * updated to latest standards revision
  * move me to non-free/misc! (fixed #14361)

 -- Paul Haggart <phaggart@debian.org>  Sat,  7 Mar 1998 15:50:01 -0500

remind (03.00.17-4) unstable; urgency=low

  * moved to non-free/misc because of -stoopid- copyright

 -- Paul Haggart <phaggart@debian.org>  Tue, 18 Nov 1997 18:28:43 -0500

remind (03.00.17-3) unstable; urgency=low

  * finally found out about virtual package 'wish', so we'll use that
    instead of a versioned TK.

 -- Paul Haggart <phaggart@debian.org>  Sun,  2 Nov 1997 09:51:33 -0500

remind (03.00.17-2) unstable; urgency=low

  * suggests tk8.0 instead of tk80 (doooh!)

 -- Paul Haggart <phaggart@debian.org>  Fri, 17 Oct 1997 12:21:18 -0400

remind (03.00.17-1) unstable; urgency=low

  * got rid of supposed bashism in debian/rules
  * fixed changelog to point to correct file
  * new upstream version (fixed bug #13412)
  * pristine sources used
  * email addr in debian/control changed from debian.com to debian.org

 -- Paul Haggart <phaggart@debian.org>  Sun, 12 Oct 1997 18:48:33 -0400

remind (3.0.16-3) unstable; urgency=low

  * using tk80 now..

 -- Paul Haggart <phaggart@debian.org>  Thu, 11 Sep 1997 08:24:02 -0400

remind (3.0.16-2) unstable; urgency=low

  * changed depends: tk42 to suggests: tk42

 -- Paul Haggart <phaggart@debian.org>  Tue, 12 Aug 1997 23:32:26 +0000

remind (3.0.16-1) unstable; urgency=low

  * recompiled for libc6
  * changed email address to @debian.org

 -- Paul Haggart <phaggart@debian.org>  Sun, 22 Jun 1997 19:46:10 -0400

remind (3.0.16-0) unstable; urgency=low

  * new upstream source

 -- Paul Haggart <phaggart@cybertap.com>  Wed, 26 Feb 1997 19:42:25 -0500

remind (3.0.15-1) unstable; urgency=low

  * Initial release

 -- Paul Haggart <phaggart@cybertap.com>  Wed, 6 Nov 1996 14:04:47 -0500
